<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Message de Bienvenue</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            text-align: center;
            margin-top: 50px;
        }

        .message-container {
            padding: 20px;
            border-radius: 10px;
            background-color: #006600;
            color: #ffffff;
            width: 50%;
            margin: auto;
        }
    </style>
</head>
<body>

    <div class="message-container">
        <h2>Bon début de formation !</h2>
        <p>Félicitations pour le commencement de votre formation. Nous sommes ravis de vous avoir parmi nous.</p>
        <p>Que cette période d'apprentissage soit enrichissante et fructueuse !</p>
    </div>

</body>
</html>



